package com.pato.bondisposter;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by pato on 11/7/16.
 */

public class BondisService {

    private static BondisService mInstance;

    public static BondisService getInstance() {
        if (mInstance == null) {
            mInstance = new BondisService();
        }
        return mInstance;
    }

    public BondisAPI mAPI;

    private BondisService() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://159.203.140.1:8080")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        mAPI = retrofit.create(BondisAPI.class);
    }

    interface BondisAPI {
        @GET("/locations/near")
        Observable<List<BondiData>> getBondisNear(@Query("lat") double lat, @Query("lng") double lng, @Query("distance") long distance);
    }
}

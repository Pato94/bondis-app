package com.pato.bondisposter;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Subscription mSubscription;
    private boolean mMapReady = false;
    private double initialLat = -34.5966538;
    private double initialLng = -58.7475495;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSubscription = createSubscription();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSubscription.unsubscribe();
        mSubscription = null;
    }

    private Subscription createSubscription() {
        final String tag = MapsActivity.class.getSimpleName();

        return Observable.interval(3, TimeUnit.SECONDS)
            .flatMap(new Func1<Long, Observable<List<BondiData>>>() {
                @Override
                public Observable<List<BondiData>> call(Long aLong) {
                    return BondisService.getInstance().mAPI
                        .getBondisNear(initialLat, initialLng, 500);
                }
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<List<BondiData>>() {
                @Override
                public void onCompleted() {
                    Log.d(tag, "Completed!");
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(tag, "Error!", e);
                }

                @Override
                public void onNext(List<BondiData> bondiDatas) {
                    Log.d(tag, "onNext!");
                    updateMarkers(bondiDatas);
                }
            });
    }

    private void updateMarkers(List<BondiData> bondiDatas) {
        if (mMapReady) {
            mMap.clear();

            LatLng initialCoords = new LatLng(initialLat, initialLng);
            mMap.addMarker(new MarkerOptions().position(initialCoords));

            for (BondiData bondiData : bondiDatas) {
                LatLng coords = new LatLng(bondiData.lat, bondiData.lng);
                mMap.addMarker(new MarkerOptions().position(coords));
            }
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMapReady = true;
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng libertad = new LatLng(initialLat, initialLng);
        mMap.addMarker(new MarkerOptions().position(libertad).title("VOS"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(libertad, 12.0f));
    }
}
